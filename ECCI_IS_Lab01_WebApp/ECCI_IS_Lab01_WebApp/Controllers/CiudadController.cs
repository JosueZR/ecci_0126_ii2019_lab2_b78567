﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using ECCI_IS_Lab01_WebApp.Models;

namespace ECCI_IS_Lab01_WebApp.Controllers
{
    public class CiudadController : Controller
    {
        private ECCI_IS_Lab01_DatosEntities db = new ECCI_IS_Lab01_DatosEntities();

        // GET: Ciudad
        public ActionResult Index()
        {
            var ciudad = db.Ciudad.Include(c => c.Pais);
            return View("Index", ciudad.ToList());
        }

        // GET: Ciudad/Details/5
        public ActionResult Details(string paisId, string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Ciudad ciudad = db.Ciudad.Find(paisId, id);
            if (ciudad == null)
            {
                return HttpNotFound();
            }
            return View("Details", ciudad);
        }

        // GET: Ciudad/Create
        public ActionResult Create()
        {
            ViewBag.PaisId = new SelectList(db.Pais, "Id", "Nombre");
            return View("Create");
        }

        // POST: Ciudad/Create
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "PaisId,Id,Nombre")] Ciudad ciudad)
        {
            if (ModelState.IsValid)
            {
                db.Ciudad.Add(ciudad);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.PaisId = new SelectList(db.Pais, "Id", "Nombre", ciudad.PaisId);
            return View("Create", ciudad);
        }

        // GET: Ciudad/Edit/5
        public ActionResult Edit(string paisId, string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Ciudad ciudad = db.Ciudad.Find(paisId, id);
            if (ciudad == null)
            {
                return HttpNotFound();
            }
            ViewBag.PaisId = new SelectList(db.Pais, "Id", "Nombre", ciudad.PaisId);
            return View("Edit", ciudad);
        }

        // POST: Ciudad/Edit/5
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "PaisId,Id,Nombre")] Ciudad ciudad)
        {
            if (ModelState.IsValid)
            {
                db.Entry(ciudad).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.PaisId = new SelectList(db.Pais, "Id", "Nombre", ciudad.PaisId);
            return View("Edit", ciudad);
        }

        // GET: Ciudad/Delete/5
        public ActionResult Delete(string paisId, string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Ciudad ciudad = db.Ciudad.Find(paisId, id);
            if (ciudad == null)
            {
                return HttpNotFound();
            }
            return View("Delete", ciudad);
        }

        // POST: Ciudad/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(string paisId,string id)
        {
            Ciudad ciudad = db.Ciudad.Find(paisId, id);
            db.Ciudad.Remove(ciudad);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using System.Windows.Input;
using System.Windows.Forms;
using System.Drawing;
using Microsoft.VisualStudio.TestTools.UITesting;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.VisualStudio.TestTools.UITest.Extension;
using Keyboard = Microsoft.VisualStudio.TestTools.UITesting.Keyboard;


namespace ECCI_IS_Lab01_WebApp.CodedUITest
{
    /// <summary>
    /// Summary description for CodedUITest1
    /// </summary>
    [CodedUITest]
    public class Estudiante2CodedUITest
    {
        public Estudiante2CodedUITest()
        {
        }

        [TestMethod]
        public void CodedUITestIngresoApp()
        {
            this.UIMap.ValidarBotonPrincipal();
            
        }

        [TestMethod]
        public void CodedUITesttituloApp()
        {
            this.UIMap.ValidarTituloPrincipal();
        }


        [TestMethod]
        public void CodedUITestEstudianteLista2()
        {

            this.UIMap.PruebaNavegarListaEstudiantes2();
            this.UIMap.ValidarColumnaApellido();
            this.UIMap.ValidarPrimerEstudianteApellido();

        }


        #region Additional test attributes

        // You can use the following additional attributes as you write your tests:

        ////Use TestInitialize to run code before running each test 
        [TestInitialize()]
        public void MyTestInitialize()
        {
            BrowserWindow.CurrentBrowser = "ie"; // "ie" “Chrome” “firefox”
            this.UIMap.InicializarExplorador();
            // To generate code for this test, select "Generate Code for Coded UI Test" from the shortcut menu and select one of the menu items.
        }

        ////Use TestCleanup to run code after each test has run
        [TestCleanup()]
        public void MyTestCleanup()
        {        
            // To generate code for this test, select "Generate Code for Coded UI Test" from the shortcut menu and select one of the menu items.
        }

        #endregion

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }
        private TestContext testContextInstance;

        public UIMap UIMap
        {
            get
            {
                if (this.map == null)
                {
                    this.map = new UIMap();
                }

                return this.map;
            }
        }

        private UIMap map;
    }
}
